# Home office equipment

- [Ikea Bekant sit/stand desk](https://www.ikea.com/us/en/catalog/products/S49022538) - Some bad reviews because the motor can sometimes die, but the long warranty means that IKEA will replace the part for free and send it to you. MAKE SURE YOU KEEP YOUR ORIGINAL RECIEPT.
- [Steelcase Leap ergonomic chair](https://www.steelcase.com/products/office-chairs/leap/)
- [This is my mac](https://www.apple.com/shop/buy-mac/macbook-pro?product=MPXT2LL/A&step=config#)
    - 13-inch MacBook Pro - Space Gray
        - 2.3GHz dual-core 7th-generation Intel Core i5 processor, Turbo Boost up to 3.6GHz
        - 16GB 2133MHz LPDDR3 memory
        - 256GB SSD storage
        - Intel Iris Plus Graphics 640
        - Two Thunderbolt 3 (USB-C) ports ([Differences between Thunderbolt 3 and USB-C](https://thunderbolttechnology.net/blog/difference-between-usb-c-and-thunderbolt-3))
- [Two Dell 24 inch Monitors](https://www.amazon.com/gp/product/B00NZTKOQI/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)
- [Mac Wireles Keyboard](https://www.apple.com/shop/product/MQ052LL/A/magic-keyboard-with-numeric-keypad-us-english-silver?fnode=56)
- [HDMI/USB/USB-C adapter](https://www.amazon.com/gp/product/B074DRW84M/ref=oh_aui_detailpage_o06_s01?ie=UTF8&psc=1)
- [HDMI/USB-C](https://www.amazon.com/gp/product/B075V68NVR/ref=oh_aui_detailpage_o06_s02?ie=UTF8&psc=1) - If you don't want to buy two of the above and save $$.
- [HDMI cables - 2 pack](https://www.amazon.com/gp/product/B06XP129CT/ref=oh_aui_detailpage_o06_s01?ie=UTF8&psc=1)
- [Logitech Ergonomic Trackball Bluetooth Mouse](https://www.amazon.com/gp/product/B0753P1GTS/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1)
- [Travel case for mouse](https://www.amazon.com/gp/product/B075JF2S7H/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
- [Bose Soundsport wireless headphones w/charging case](https://www.amazon.com/gp/product/B01L7PWBRG/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)
- [Yeti Microphone](https://www.amazon.com/Blue-Yeti-USB-Microphone-Blackout/dp/B00N1YPXW2/ref=sr_1_3?s=electronics&ie=UTF8&qid=1523303914&sr=1-3&keywords=yeti+microphone&dpID=310-tLIc6uL&preST=_SY300_QL70_&dpSrc=srch) - this is great for meetings at home so you don't have to use headphones and are easy to find cheaper second hand.
- [Timbuk2 Spire laptop bag](https://www.amazon.com/gp/product/B00M480ON4/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)
- [Tech21 hardshell case](https://www.apple.com/shop/product/HLQB2ZM/A/tech21-13-pure-clear-case-for-macbook-pro-with-thunderbolt-3-usb-c?fnode=668d7d44c490d2870faac0bfadfa758fff9dbb94dec4bdb2842766bbb787f46824bc825c0264e69255393d0a4997f8a48ffbfc5e18b1459d729a95f3dd8429786103a9c21e784151353102980f1776a487e6db43b5fd34cb69679767edd680e8f7ced3e9baf7b0537aa2f9ded3e156c8)

# Mac setup guide

When I get a new mac, or run an erase and install on an old one, this is how I personally like to have it set up.

## System Preferences

### Dock:
- Right click on your Downloads folder on the Dock > View content as list 
- Preferences:

![Dock Preferences](/images/dockprefs.png "Dock Preferences")

### Mouse / Trackpad / Keyboard:
- Keyboard > Key Repeat > Fast
- Keyboard > Delay Until Repeat > Short
- Trackpad > Point & Click > Tap to click
- Trackpad > Point & Click > Tracking speed > Fast
- Trackpad > More Gestures > Mission Control > yes
- Mouse > Tracking speed > Fast

### Date & Time Preferences
- Clock > Show date and time in menu bar > yes

### Mission Control
- Mission Control > Dashboard > Off (This may not persist. If it doesn't, follow the instructions [here](http://osxdaily.com/2007/03/14/how-to-completely-disable-dashboard/))
- Mission Control > Uncheck "Displays have different spaces" - this will ensure your wallpaper doesn't reset to default one each time you reboot if you have multiple displayes.
- Mission Control > Hot Corners

	I personally like to use hot corners, lots of people find this annoying to use and it takes a minute to get used to it, but if paired with Spectacle, it makes moving things around and switching windows and apps so much easier. This is how I have mine configured:

![Hot Corners Settings](/images/hotcorners.png "Hot Corners Settings")

## Development

### Xcode & Command Line Tools

You don't need to install Xcode in order to install the Command Line Tools (which you need in order to be able to use things like Homebrew), if you're running macOS 10.9 or higher, open your terminal and run ```xcode-select --install```

## Terminal

- [Homebrew](https://brew.sh/)
- [Ruby](https://www.ruby-lang.org/en/)

[This guide](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-and-set-up-a-local-programming-environment-on-macos) will run through everything you need to get started with using Homebrew and ruby. You can skip the command line tools bit since we already did that.

- [Git](https://git-scm.com/doc) - to install git, do essentially the same thing you did for ruby:

```
➜  ~ brew install git
==> Downloading https://homebrew.bintray.com/bottles/git-2.17.0.high_sie
############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################### 100.0%
==> Pouring git-2.17.0.high_sierra.bottle.tar.gz
==> Caveats
Bash completion has been installed to:
  /usr/local/etc/bash_completion.d

zsh completions and functions have been installed to:
  /usr/local/share/zsh/site-functions

Emacs Lisp files have been installed to:
  /usr/local/share/emacs/site-lisp/git
==> Summary
🍺  /usr/local/Cellar/git/2.17.0: 1,497 files, 35.6MB
```
- 



- [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

oh-my-zsh plugins:
```
➜  ~ vi ~/.zshrc

plugins=(
  brew git docker compleat gem lol nyan osx rails ruby rvm ssh-agent sudo web-search
)
```


Some of my favourite terminal/zsh themes:

- [Dracula](https://draculatheme.com/)
- [Solarized](https://xuri.me/2013/11/09/use-solarized-color-scheme-in-os-x-terminal.html)
- [Spaceship](https://medium.com/@denysdovhan/a-big-update-of-spaceship-zsh-theme-a705776ac6b5)


## Apps

### Free

- [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
   - Extensions
      - uBlock Origin
      - Pocket
      - Momentum
      - Mercury Reader
- [Firefox](https://www.mozilla.org/en-US/firefox/new/) (For testing and tinkering)
- [Google Drive](https://www.google.com/drive/download/)
- [Dropbox](https://www.dropbox.com/)
- [Sublime Text](https://www.sublimetext.com/) 
    - [Install package control](https://packagecontrol.io/installation) before you do anything else.
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [Docker](https://www.docker.com/community-edition)
- [Spectacle App](https://www.spectacleapp.com/)
- [Simplenote](https://simplenote.com/)
- [Spotify](https://www.spotify.com/us/)
- [VLC](https://www.videolan.org/vlc/index.html)
- [Slack](https://slack.com/)
- [Zoom](https://zoom.us/)
   - Set your audio to mute upon joining
   - Set your video to turn off upon joining
- [Limechat IRC client](http://limechat.net/mac/)

This is how I set up my server and also includes my channel list:

![Limechat Settings](/images/limechat.png "Limechat Settings")

### Not free

- [Wallpaperer](https://itunes.apple.com/us/app/wallpaperer-for-reddit/id1102248738?mt=12)
- [iStat Menus](https://bjango.com/mac/istatmenus/)
- [CleanMyMac](https://macpaw.com/cleanmymac)

## Security

- FileVault - [Encrypt your hard drive](https://support.apple.com/en-us/HT204837)
- [Dashlane](https://www.dashlane.com/)
- [Allow apps downloaded from anywhere](https://www.imore.com/how-open-apps-unidentified-developers-mac):

Open your terminal and run ```sudo spctl --master-disable``` and then restart system preferences. 


## Bonus - Design Stuff

- [Sketch](https://www.sketchapp.com/) + [Plugins](https://www.sketchapp.com/extensions/plugins/)
- [Adobe Creative Suite](http://www.adobe.com/creativecloud.html)
- [Coolers Chrome Extension](https://chrome.google.com/webstore/detail/coolors/mgajdijnadmkoapndjaacifihabcnfgd)